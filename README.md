# The way of the NYMJA 🥷🏼

Curated wiki of relevant NYMformation by Pinapple Proxy crew. Probably nothing 👀 

---

**🗺️Menu for today**

---

## 🥐**How-to guides**

[Minymja - NYM token holder](The%20way%20of%20the%20NYMJA%20%F0%9F%A5%B7%F0%9F%8F%BC%208e4b19ff876a43b9bb197b22a2a5320b/Minymja%20-%20NYM%20token%20holder%204ad2202a8e504c3dbee6a0a30bd33213.md)

[DEX pools and token bridging **from ETH to NYM chain - using the cosmos gravity bridge**](The%20way%20of%20the%20NYMJA%20%F0%9F%A5%B7%F0%9F%8F%BC%208e4b19ff876a43b9bb197b22a2a5320b/DEX%20pools%20and%20token%20bridging%20from%20ETH%20to%20NYM%20chain%206e8b815074954b7ea22ba15f594483d2.md)

[Tinymja - NYM ecosystem supporter](The%20way%20of%20the%20NYMJA%20%F0%9F%A5%B7%F0%9F%8F%BC%208e4b19ff876a43b9bb197b22a2a5320b/Tinymja%20-%20NYM%20ecosystem%20supporter%20ecb1b2cd6489493a8149cc035ea8f4db.md)

[Virtual Private Server setup (VPS -draft)](The%20way%20of%20the%20NYMJA%20%F0%9F%A5%B7%F0%9F%8F%BC%208e4b19ff876a43b9bb197b22a2a5320b/Virtual%20Private%20Server%20setup%20(VPS%20-draft)%20b77442ecc0334c3c894dcbf29ac4d698.md)

[Nymchuck Norris - NYM mixnode operator (plain code setup)](The%20way%20of%20the%20NYMJA%20%F0%9F%A5%B7%F0%9F%8F%BC%208e4b19ff876a43b9bb197b22a2a5320b/Nymchuck%20Norris%20-%20NYM%20mixnode%20operator%20(plain%20code%208a1eed02ef1942e881ae78dc3ae7713d.md)

[Nymj**ackie Chan** - NYM mixnode ubuntu server setup](The%20way%20of%20the%20NYMJA%20%F0%9F%A5%B7%F0%9F%8F%BC%208e4b19ff876a43b9bb197b22a2a5320b/Nymjackie%20Chan%20-%20NYM%20mixnode%20ubuntu%20server%20setup%20903d42371d00423f8ff1855e7d8a8588.md)

[Build your own hardware  (draft) ](The%20way%20of%20the%20NYMJA%20%F0%9F%A5%B7%F0%9F%8F%BC%208e4b19ff876a43b9bb197b22a2a5320b/Build%20your%20own%20hardware%20(draft)%2094506d531611467abc9c492b4ed303ee.md)

---

## 🍍Blog articles & essays

[Dignity on the internet as a global citizen](The%20way%20of%20the%20NYMJA%20%F0%9F%A5%B7%F0%9F%8F%BC%208e4b19ff876a43b9bb197b22a2a5320b/Dignity%20on%20the%20internet%20as%20a%20global%20citizen%20b53b115619c74960963f9275a2e54416.md)

[Our favourite things about Nym’s tokenomics (draft)](The%20way%20of%20the%20NYMJA%20%F0%9F%A5%B7%F0%9F%8F%BC%208e4b19ff876a43b9bb197b22a2a5320b/Our%20favourite%20things%20about%20Nym%E2%80%99s%20tokenomics%20(draft%20c5b0e91f081241eda4b1ce38195beaa3.md)

[What happens at the gates, stays at the gates ](The%20way%20of%20the%20NYMJA%20%F0%9F%A5%B7%F0%9F%8F%BC%208e4b19ff876a43b9bb197b22a2a5320b/What%20happens%20at%20the%20gates,%20stays%20at%20the%20gates%2059bdcc6ecd8d453e82af66f468adb81a.md)

---

## 🛡️**Official NYM links & documents**

**🌐[Nymtech website](https://nymtech.net)**

**📄[NYM lite paper](https://nymtech.net/nym-litepaper.pdf)**

**🔭[NYM whitepaper](https://nymtech.net/nym-whitepaper.pdf)**

**🪙[NYM token economics paper](https://nymtech.net/nym-cryptoecon-paper.pdf)**

---

## ⭐**NYM Socials**

📺[Youtube](https://www.youtube.com/@Nymtech)

🦜[Twitter](https://twitter.com/nymproject)

📱[Telegram](https://t.me/nymchan)

👾[Discord](https://discord.gg/nym)

📖[Medium](https://medium.com/nymtech)

🔮[Github](https://github.com/nymtech)

💰[Coinmarketcap](https://coinmarketcap.com/currencies/nym/)

---

## ⚡**NYM-DEV-TOOLS**

[🔎💯Nym githbub repo](https://github.com/nymtech/nym/tree/nym-connect-v1.1.2)

☑️ [NYM **Mixnet** Explorer](https://explorer.nymtech.net)  (OG explorer of the mixnet)

🕉️[Mixnet explorer](https://mixnet.explorers.guru) by [Nodes Guru](https://nodes.guru)   - ⚠️ Keep in mind the view this explorer has two sides- one for the mixnet and one for the NYX blockchain ⚠️

[⛓️ NYM SDK](https://www.npmjs.com/package/@nymproject/sdk)

---

## 💻Services and apps powered by NYM mixnet

☑️[Is-NYM-up?](https://isnymup.com) - Quick mixnet status checker by NTV.

💬[NYM mixnet chat app](https://chat-demo.nymtech.net) - demonstrative private p2p chat app built with webassembly.

🕶️[Pastenym](https://pastenym.ch/#/) - anyonymus file sharing - supporting images by [No Trust Verify](https://nym.notrustverify.ch).

👩🏼‍💻[Live mixnet](https://status.notrustverify.ch/grafana/d/CW3L7dVVk/nym-mixnet?orgId=1) - detailed mixnet dashboard by NTV.

🕉️[Node tracker](https://t.me/NodesGuru_bot) - node checker telegram bot by NG.

---

## 🍍Mixnode ❤️‍🔥

[NoTrustVerify](https://nym.notrustverify.ch) - Professional Infrastructure🇨🇭 | Fair Fees | Nym Contributor | [https://t.me/notrustverify](https://t.me/notrustverify)
◼️[NTV1 node](https://mixnet.explorers.guru/mixnode/4yRfauFzZnejJhG2FACTVQ7UnYEcFUYw3HzXrmuwLMaR)
◾[NTV2 node](https://mixnet.explorers.guru/mixnode/4yRfauFzZnejJhG2FACTVQ7UnYEcFUYw3HzXrmuwLMaR)

[Amphibios 9🍍🇬🇪- th](https://mixnet.explorers.guru/mixnode/RYDYNZNwZfeZs87TLhn4dWAK9xax3eLdMJ1fzJnqFvU)is mixnode was launched in Georgia - one of the world's most cryptocurrency-friendly countries!

---

## 🕵🏼 ㊙️Privacy repos & further links

[🕶️**web3privacy.now**](https://github.com/Msiusko/web3privacy)

🗨️[Secret network ecosystem](https://scrt.network/ecosystem/dapps)

💠[Universal Privacy Alliance](https://privacyalliance.com)

- **🗺️privacy tech landscape to browse**
    
    ![Web3privacy landscape now.png](The%20way%20of%20the%20NYMJA%20%F0%9F%A5%B7%F0%9F%8F%BC%208e4b19ff876a43b9bb197b22a2a5320b/Web3privacy_landscape_now.png)
    

---
